#!/bin/bash
# Install prerequisites
if [ ! -f /usr/bin/python3 ]; then
        sudo apt update
        sudo apt install -y python3
fi
if [ ! -f /usr/bin/pip3 ]; then
        sudo apt update
        sudo apt install -y python3-pip
fi

for module in pandas inquirer; do
        python3 -c "import $module"
        if [ $? -ne 0 ]; then
                pip3 install inquirer pandas
        fi
done

# Run Trainerator!
python3 trainerator.py
