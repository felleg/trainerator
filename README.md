# Trainerator

Use this to create custom workout programs for your athletes.

## Requirements

This program requires:

- Python 3
- inquirer (a Python module)

## How to run this program

A script has been created for your ease of use, it will install all dependencies you need on first execution.
You can run it like this:

```bash
./run_trainerator.sh
```

## Adding athletes

The configuration files for athletes are stored in `athletes/config`. Follow the style from
`example_athlete.json`:

```json
{
        "name" : "Example Athlete",
        "days" : [
                "Tuesday",
                "Friday",
                "Sunday"
        ],
        "meeting" : "Wednesday 15h00"
}
```

- `name`: The name of the athlete
- `days`: List of days when the athlete is expected to train in a week
- `meeting`: Date and time when the athlete will meet their trainer every week.

## How to add new exercises

All exercise categories are stored in `exercises/exercises.csv`. Here is an example of an exercise list,
notice the first line in `exercises.csv` denotes column names

```txt
exercise_name,group_number,exercise_category,training_category,include
EXERCISE 1,1,Exercise category 1,Training category 1,x
EXERCISE 2,2,Exercise category 1,Training category 1,x
EXERCISE 3,1,Exercise category 2,Training category 1,
EXERCISE 4,2,Exercise category 2,Training category 1,x

EXERCISE 5,1,Exercise category 3,Training category 2,x
EXERCISE 6,1,Exercise category 3,Training category 2,x
EXERCISE 7,2,Exercise category 3,Training category 2,x
EXERCISE 8,1,Exercise category 4,Training category 2,x
EXERCISE 9,2,Exercise category 4,Training category 2,x

EXERCISE 10,1,Exercise category 5,Training category 3,x
EXERCISE 11,2,Exercise category 5,Training category 3,x
EXERCISE 12,3,Exercise category 5,Training category 3,x
EXERCISE 13,1,Exercise category 6,Training category 3,x
EXERCISE 14,2,Exercise category 6,Training category 3,x
EXERCISE 15,1,Exercise category 7,Training category 3,x
EXERCISE 16,3,Exercise category 7,Training category 3,x
EXERCISE 17,1,Exercise category 8,Training category 3,
EXERCISE 18,2,Exercise category 8,Training category 3,
```

### Definition of columns in exercises.csv
- `exercise_name`: The name of the exercise, the number of repetitions to execute, etc.
- `group_number`: The group number of the exercise. In a single training session, only one group number is used to
  ensure all exercises are compatible. The `group_number` used in a session is determined at random
- `exercise_category`: The exercise category. In a single training session, one random exercise per
  `exercise_category` will be chosen.
- `training_category`: The training category of the exercise. In the program, exercises from a training
  category will be scheduled for a fix number of weeks (defined by `WEEKS_PER_TRAINING_CATEG` in
  `trainerator.py`), before moving on to the next category.
- `include`: Should this exercise be included in the program? If so, apply an `X` character (or any other
  character) to mark it as included. This will allow to to populate a huge bank while being flexible of what
  exercises should be applied each time you run Trainerator.

# NOTES
**The chronology of exercise and training categories will be determined in what order they appear in
exercises.csv.** For example, if you want a training category order of `Off season` -> `In season` ->
`Competitive`, these categories should appear in that order in `exercises.csv`. The same logic applies for
exercise categories of a single session. In the future, a configuration file could be created to enforce this
more clearly, but hopefully this "intuitive" approach should work a for a while (the developer hopes).

**To increase the probability of an exercise**, simply copy-paste it multiple times in the csv file of
exercises.

# TODO
- Add pipeline job to run pylint over `trainerator.py` for codestyle
- Add pipeline job to run unit tests
