# [1.5.0](https://gitlab.com/felleg/trainerator/compare/v1.4.1...v1.5.0) (2020-07-06)


### Features

* include column, remove competition, enforce 1 categ per program, quality of life ([c864d55](https://gitlab.com/felleg/trainerator/commit/c864d55f76310352dbd630f169e5756eddbb879f))

## [1.4.1](https://gitlab.com/felleg/trainerator/compare/v1.4.0...v1.4.1) (2020-05-01)


### Bug Fixes

* remove extra space in date line of output ([c41a51e](https://gitlab.com/felleg/trainerator/commit/c41a51efc637bccd52a1b37e64ef0c5128db5284))

# [1.4.0](https://gitlab.com/felleg/trainerator/compare/v1.3.1...v1.4.0) (2020-04-28)


### Features

* in output, separate 1 session in multiple lines, for ease of reading ([2d0db7a](https://gitlab.com/felleg/trainerator/commit/2d0db7abdd4c66c7c37e674e77fa37bbc81cacd9))

## [1.3.1](https://gitlab.com/felleg/trainerator/compare/v1.3.0...v1.3.1) (2020-04-28)


### Bug Fixes

* remove competition day in summary if no competition ([0721710](https://gitlab.com/felleg/trainerator/commit/0721710dddc0891241bce58a3e76e72cab7fd6b8))

# [1.3.0](https://gitlab.com/felleg/trainerator/compare/v1.2.0...v1.3.0) (2020-04-28)


### Features

* add question to determine if plan for competition or not ([e3c1c5e](https://gitlab.com/felleg/trainerator/commit/e3c1c5ea897f90bdd578bcca987a7660ab678512))

# [1.2.0](https://gitlab.com/felleg/trainerator/compare/v1.1.0...v1.2.0) (2020-04-27)


### Features

* add menu to select specific exercises file ([2b4a80f](https://gitlab.com/felleg/trainerator/commit/2b4a80f525f6dc10aa7122d11976300137036e03))

# [1.1.0](https://gitlab.com/felleg/trainerator/compare/v1.0.3...v1.1.0) (2020-04-27)


### Features

* add menu to select specific exercises file ([2b4a80f](https://gitlab.com/felleg/trainerator/commit/2b4a80f525f6dc10aa7122d11976300137036e03))

## [1.0.3](https://gitlab.com/felleg/trainerator/compare/v1.0.2...v1.0.3) (2020-04-26)


### Bug Fixes

* training sessions will only be outputed for days in the future ([1fc5acb](https://gitlab.com/felleg/trainerator/commit/1fc5acb6832da3e60f9edd7a378b40d5c8b31d06))

## [1.0.2](https://gitlab.com/felleg/trainerator/compare/v1.0.1...v1.0.2) (2020-04-26)


### Bug Fixes

* **run_trainerator.sh:** fix run script to install python dependencies ([e3f6e56](https://gitlab.com/felleg/trainerator/commit/e3f6e563a67ac8950dd90d6574d489c1980b439d))

## [1.0.1](https://gitlab.com/felleg/trainerator/compare/v1.0.0...v1.0.1) (2020-04-26)


### Bug Fixes

* enforce competition date in the future ([50eedf9](https://gitlab.com/felleg/trainerator/commit/50eedf9717486c3fb0b10aa307d8df33aabb4689))

# 1.0.0 (2020-04-26)


### Bug Fixes

* programs exclude dates that are in the past when executing ([732f53f](https://gitlab.com/felleg/trainerator/commit/732f53f9d16cfbd7c4b2b48b72f3227a44bab08b))
* **trainerator:** add cycling of group_numbers, remove random group_numbers each session ([8f0a877](https://gitlab.com/felleg/trainerator/commit/8f0a8772cda0479ce57f730067f07565cf6933fa))


### Features

* **generate_all_trainers:** add script to generate all trainers ([6c8dfe4](https://gitlab.com/felleg/trainerator/commit/6c8dfe4c092fdf87183fadfa98b988c8e6cc45ec))
* change format of exercise and athlete to allow realistic training programs ([4cee71a](https://gitlab.com/felleg/trainerator/commit/4cee71ae5ae5b4b7f2e9977ce87c852d0a2fc154))
* first working version of trainerator ([52e3c7e](https://gitlab.com/felleg/trainerator/commit/52e3c7e642cb0033711cd1c25fdb0c886391e0cb))
* **trainerator:** overhaul of trainerator, new exercises and athlete format ([38f24e9](https://gitlab.com/felleg/trainerator/commit/38f24e9339d4ba13678ec4f14a267a71c8f968d4))
