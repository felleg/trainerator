"""
This program will generate a training program for an athlete. Some people pay good money to get
personalized training programs; with this tool, you'll be able to generate as many programs as you
want for free!

Copyright: Felix Leger, 2020
https://gitlab.com/felleg
"""

import datetime
import json
import os
import random
import pandas
import inquirer
import inquirer.themes
import dateutil.relativedelta

DEFAULT_PROGRAM_LENGTH = 5

# Convert days to int, and vice versa
DAY_INT_TO_STR = {0 : "Monday",
                  1 : "Tuesday",
                  2 : "Wednesday",
                  3 : "Thursday",
                  4 : "Friday",
                  5 : "Saturday",
                  6 : "Sunday"
                 }

DAY_STR_TO_INT = {"monday" : 0,
                  "Monday" : 0,
                  "tuesday" : 1,
                  "Tuesday" : 1,
                  "wednesday" : 2,
                  "Wednesday" : 2,
                  "thursday" : 3,
                  "Thursday" : 3,
                  "friday" : 4,
                  "Friday" : 4,
                  "saturday" : 5,
                  "Saturday" : 5,
                  "sunday" : 6,
                  "Sunday" : 6
                 }

def parse_athlete(ath):
    '''
    This function will parse the .json file for the selected athlete (ath) in athletes/config/
    The json is returned as a dict.

    PARAMETERS:
    -----------
    ath: str
        Filename that contains the JSON athlete config

    RETURNS:
    --------
    A dictionary object containing the JSON athlete config
    '''
    with open(ath) as file:
        return json.load(file)

def parse_exercises(ex):
    '''
    This function will read `ex` (a string pointing to a .csv file) and store its content them in a
    dataframe. It will only include exercises that are marked for inclusion, using the "include"
    column in the csv.

    PARAMETERS:
    -----------
    ex: str
        Filename that contains the csv list of exercises

    RETURNS:
    --------
    The dataframe created from `ex` .csv file
    '''
    dataframe = pandas.read_csv(ex, keep_default_na=False)
    return dataframe[dataframe.include != ""]

def format_datetime(d_t):
    '''
    This function is used to format a datetime into a standard string format readable by humans

    PARAMETERS:
    -----------
    d_t : datetime
        A time in datetime format

    RETURNS:
    --------
    A string with this format: "dow month date"
    For example, "Sunday April 26"
    '''
    return "{} {} {}".format(
        d_t.strftime("%a"),
        d_t.strftime("%B"),
        d_t.strftime("%d")
    )


def generate_session(exercise_df, training_category, gr_number):
    '''
    This function generate a string representing the exercises that the athlete should perform in a
    single session.

    PARAMETERS:
    -----------
    exercise_df : pandas.dataframe
        The dataframe containing all exercises from the "exercises/" folder
    training_category : str
        The training category from exercises_df for which to generate a training session. This will
        narrow down the choice of exercises for this session.
    gr_number : int
        The group number of exercises to perform in this session. Along with `training_category`,
        this will narrow down the selection of exercises.

    RETURNS:
    --------
    This function returns the list of exercises to perform in a single session
    Here is an example of the format:
    "Exercise 1  ||  Exercise 2  ||  Exercise 3"
    "Snatch Ex 1,12/4  ||  Clean and Jerk Ex 4,12/4  ||  Snatch Ex 5,12/4"
    '''

    exercise_subdf = exercise_df[exercise_df.training_category == training_category]
    # All exercises in a session must have the same group number
    exercise_subdf = exercise_subdf[exercise_subdf.group_number == gr_number]

    # The string that will contain the formatted list of exercises to perform in a single session
    session_str = "\n" + 8*" "

    # Take a random exercise for each exercise_category with this group number
    for index, exercise_category in enumerate(exercise_subdf.exercise_category.unique()):
        session_str += random.choice(list(exercise_subdf[exercise_subdf.exercise_category == \
                    exercise_category].exercise_name))
        # Create separation between exercises on the same line if there are exercises left to write
        if index < len(exercise_subdf.exercise_category.unique()) - 1:
            session_str += "\n" + 8*" "
        else:
            session_str += "\n"
    return session_str

if __name__ == "__main__":
    # Keep the moment of execution of trainerator in memory
    NOW = datetime.datetime.now()

    # Prompt the user to select an athlete and exercise file from the list
    ANSWERS = inquirer.prompt([
        inquirer.List("athlete",
                      message="Please select your athlete",
                      choices=[i for i in os.listdir("athletes/config") if i[-5:] == ".json"],
                      ),
        inquirer.List("exercise_file",
                      message="What exercise file should I use?",
                      choices=[i for i in os.listdir("exercises/") if i[-4:] == ".csv"],
                      )
    ], theme=inquirer.themes.GreenPassion())

    ATHLETE_FILE = os.path.join("athletes/config", ANSWERS["athlete"])
    ATHLETE = parse_athlete(ATHLETE_FILE)
    EXERCISE_FILE = os.path.join("exercises", ANSWERS["exercise_file"])
    EXERCISES = parse_exercises(EXERCISE_FILE)
    TRAINING_CATEGORIES = list(EXERCISES["training_category"].unique())

    # If exercise file contains many categories, force user to select one
    if len(TRAINING_CATEGORIES) > 1:
        ANSWERS2 = inquirer.prompt([
            inquirer.List("training_categ",
                          message="Please select a training category for your athlete",
                          choices=TRAINING_CATEGORIES,
                          ),
        ], theme=inquirer.themes.GreenPassion())
        TRAINING_CATEGORY = ANSWERS2["training_categ"]
    else:
        assert len(TRAINING_CATEGORIES) == 1, \
                "Error, expecting TRAINING_CATEGORIES to be of length 1 here"
        TRAINING_CATEGORY = TRAINING_CATEGORIES[0]


    # Athlete will train during number of weeks determined by user
    PROGRAM_LENGTH = \
            input("How many weeks should be scheduled in this program " + \
            "(default {}): ".format(DEFAULT_PROGRAM_LENGTH))
    PROGRAM_LENGTH = DEFAULT_PROGRAM_LENGTH if PROGRAM_LENGTH == '' \
                               else int(PROGRAM_LENGTH)
    print("Select {} weeks.".format(PROGRAM_LENGTH))

    END_DATETIME = NOW + \
        dateutil.relativedelta.relativedelta(weeks=PROGRAM_LENGTH)

    # Go back in time PROGRAM_LENGTH, to start a loop that will create the
    # training program
    BEGINTRAINING_DATETIME = END_DATETIME - \
        dateutil.relativedelta.relativedelta(weeks=PROGRAM_LENGTH)

    # Store all program file content into a string to into into a file at the end of Trainerator
    PROGRAM_FILE_CONTENT = ""
    PROGRAM_FILE_CONTENT += "{}\n\n\n".format("="*50)
    PROGRAM_FILE_CONTENT += "ATHLETE: {}\n".format(ATHLETE["name"])
    PROGRAM_FILE_CONTENT += "MEETING: {}\n".format(ATHLETE["meeting"])
    PROGRAM_FILE_CONTENT += "\n\n{}\n\n".format("="*50)

    # Loop over all weeks to create program
    SESSION_COUNTER = 0     # Counter for how many sessions are performed by athlete
    for week_index in range(PROGRAM_LENGTH):
        # Loop over the 7 days of the week
        for day_index in range(7):
            # We calculate what is the first day of the next week towards the competition date
            day_in_week = BEGINTRAINING_DATETIME + \
                dateutil.relativedelta.relativedelta(weeks=week_index, days=day_index)
            if day_in_week.strftime("%A") in ATHLETE["days"]:
                # Only create sessions for days that are in the future
                if day_in_week > NOW:
                    # Rotate group_number to use
                    list_group_numbers = list(EXERCISES[EXERCISES.training_category == \
                        TRAINING_CATEGORY].group_number.unique())

                    # We cycle through the group numbers after each session.
                    group_number = list_group_numbers[SESSION_COUNTER % len(list_group_numbers)]
                    # We fetch "random" exercises for the athlete ot perform during this day
                    PROGRAM_FILE_CONTENT += "{} :{}\n".format(
                        format_datetime(day_in_week),
                        generate_session(EXERCISES,
                                         TRAINING_CATEGORY,
                                         group_number)
                    )
                    SESSION_COUNTER += 1
            # Output separation line between weeks (6 = Saturday)
            if int(day_in_week.strftime("%w")) == 6 and day_in_week > NOW:
                PROGRAM_FILE_CONTENT += "-"*70 + "\n\n"

    # Create output program file
    os.makedirs(os.path.join("athletes/output"), exist_ok=True)
    PROGRAM_FILE_NAME = "athletes/output/{}.txt".format(ANSWERS["athlete"].split(".")[0])
    PROGRAM_FILE = open(PROGRAM_FILE_NAME, "w")
    PROGRAM_FILE.write(PROGRAM_FILE_CONTENT)
    print("Generated", PROGRAM_FILE_NAME)
    PROGRAM_FILE.close()
